// DOM Selectin
// document.getElementById()
const judul = document.getElementById('judul');
judul.style.color = 'yellow';
judul.style.backgroundColor = 'grey';
judul.innerHTML = 'Sri Yustinia';

// document.getElementsByTagName()
// -> HTMCollections
const p = document.getElementsByTagName('p');

    for( let i = 0; i < p.length; i++ ){
        p[i].style.backgroundColor = 'lightblue';
    }

const h1 = document.getElementsByTagName('h1')[0];
h1.style.fontSize = '50px';


// document.getElementsByClassName()
// -> HTMLCollection
const p1 = document.getElementsByClassName('p1');
p1[0].innerHTML = 'Ini diubah dari javascript';